const getSum = (str1, str2) => {
  if(typeof(str1) !== 'string' || typeof(str2) !== 'string' || isNaN(+str1) || isNaN(+str2))
    return false;
  return (+str1 + +str2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  for(let post of listOfPosts) {
    if(post.author === authorName) {
        posts++;
    }
    if(post.comments !== undefined) {
      post.comments.forEach(e => {
        if(e.author === authorName)
          comments++;
      });
    }
  }
  return `Post:${posts},comments:${comments}`;
};

const tickets=(people)=> {
  let d25 = 0;
  let d50 = 0;
  for(let p of people) {
    if(p > 25 && d25 < 1)
      return 'NO';
    switch(p) {
      case 25: {
        d25++;
        break;
      }
      case 50 : {
        d50++;
        d25--;
        break;
      }
      case 100: {
        if(d50 < 1) {
          if(d25 < 3)
            return 'NO';
          d25 -= 3;
        } else {
          d50--;
          d25--;
        }
        break;
      }
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
